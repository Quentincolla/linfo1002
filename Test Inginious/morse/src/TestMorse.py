#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
import sys

import CorrMorse as correct
import morse as student


class TestAbs(unittest.TestCase):
    
    def test_empty(self):
        string = ''
        rep = ("Votre fonction ne fonctionne pas correctement lorsqu'elle est appelée avec un string vide. (La réponse attendue est ce même string vide.)")
        try: 
            student_ans=student.morse(string)
        except:
            e = sys.exc_info()[0]
            self.fail("Votre fonction a provoqué l\'exception "+
                        str(e)+" avec comme argument "+str(string))
        correct_ans = correct.morse(string)
        self.assertEqual(student_ans, correct_ans, rep)


    def test_mot_fr_morse(self):
        args=['SOS', 'test', 'BONJOUR', 'cHaT']
        rep = _("Votre fonction a rencontré une erreur en traduisant un mot du français au morse.\n \
        Elle a retourné {} en étant appelée avec {} comme argument alors que la réponse attendue était {}")
        for string in args:
            try:
                student_ans=student.morse(string)
            except:
                e = sys.exc_info()[0]
                self.fail("Votre fonction a provoqué l\'exception "+
                         str(e)+" avec comme argument "+str(string))
            correct_ans=correct.morse(string)
            self.assertEqual(student_ans, correct_ans,
                             rep.format(student_ans,string,correct_ans))
            
    def test_mot_morse_fr(self):
        args=['...|___|...', '_|.|...|_', '_...|___|_.|.___|___|.._|._.', '_._.|....|._|_']
        rep = _("Votre fonction a rencontré une erreur en traduisant un mot du morse au français.\n \
            Elle a retourné {} en étant appelée avec {} comme argument alors que la réponse attendue était {}")
        for string in args:
            try:
                student_ans=student.morse(string)
            except:
                e = sys.exc_info()[0]
                self.fail("Votre fonction a provoqué l\'exception "+
                         str(e)+" avec comme argument "+str(string))
            correct_ans=correct.morse(string)
            self.assertEqual(student_ans, correct_ans,
                             rep.format(student_ans,string,correct_ans))
            
    def test_phrase_fr_morse(self):
        args=['ceci est une longue phrase']
        rep = _("Votre fonction a rencontré une erreur en traduisant une phrase du français au morse.\n \
            Elle a retourné {} en étant appelée avec {} comme argument alors que la réponse attendue était {}")
        for string in args:
            try:
                student_ans=student.morse(string)
            except:
                e = sys.exc_info()[0]
                self.fail("Votre fonction a provoqué l\'exception "+
                         str(e)+" avec comme argument "+str(string))
            correct_ans=correct.morse(string)
            self.assertEqual(student_ans, correct_ans,
                             rep.format(student_ans,string,correct_ans))

    def test_phrase_morse_fr(self):
        args=['_._.|.|_._.|..||.|...|_||.._|_.|.||._..|___|_.|__.|.._|.||.__.|....|._.|._|...|.']
        rep = _("Votre fonction a rencontré une erreur en traduisant une phrase du morse au français.\n \
            Elle a retourné {} en étant appelée avec {} comme argument alors que la réponse attendue était {}")
        for string in args:
            try:
                student_ans=student.morse(string)
            except:
                e = sys.exc_info()[0]
                self.fail("Votre fonction a provoqué l\'exception "+
                         str(e)+" avec comme argument "+str(string))
            correct_ans=correct.morse(string)
            self.assertEqual(student_ans, correct_ans,
                             rep.format(student_ans,string,correct_ans))

    def test_caracteres_speciaux(self):
        args=['éèê.!']
        rep = _("Votre fonction a rencontré une erreur en étant appelée avec des caractères spéciaux.\n \
            Elle a retourné {} en étant appelée avec {} comme argument alors que la réponse attendue était {}")
        for string in args:
            try:
                student_ans=student.morse(string)
            except:
                e = sys.exc_info()[0]
                self.fail("Votre fonction a provoqué l\'exception "+
                         str(e)+" avec comme argument "+str(string))
            correct_ans=correct.morse(string)
            self.assertEqual(student_ans, correct_ans,
                             rep.format(student_ans,string,correct_ans))
            
if __name__ == '__main__':
    unittest.main()