#!/usr/bin/python3
# -*- coding: utf-8 -*-

def morse(string):
    fr_morse = {'a' : '._', 'b' : '_...', 'c' : '_._.', 'd' : '_..', 'e' : '.', \
            'f' : '.._.', 'g' : '__.', 'h' : '....', 'i' : '..', 'j' : '.___',  \
            'k' : '_._', 'l' : '._..', 'm' : '__', 'n' : '_.', 'o' : '___', \
            'p' : '.__.', 'q' : '__._', 'r' : '._.', 's' : '...', 't' : '_', \
            'u' : '.._', 'v' : '..._', 'w' : '.__', 'x' : '_.._', 'y' : '_.__', 'z': '__..'}
    #Create morse -> fr dictionary
    morse_fr = {}
    for key in fr_morse:
        morse_fr[fr_morse[key]] = key

    translation = ''

    if string == '':
        return string

    elif string[0] in '._|':
    #Morse -> Français
        string = string.split('||')
        for word in string:
            word = word.split('|')
            for letter in word:
                translation += morse_fr[letter]
            translation += ' '
        final_translation = translation.rstrip()

    else:
    #Français -> Morse
        string = string.split(' ')
        for word in string:
            for letter in word:
                if letter not in 'éèêàâîïôöùûü&!.?/\+=-*@~':
                    translation += fr_morse[letter.lower()]
                elif letter in 'éèê':
                    translation += fr_morse['e']
                elif letter in 'àâ':
                    translation += fr_morse['a']
                elif letter in '!.':
                    pass
                translation += '|'
            translation += '|'
        final_translation = ''
        for e in translation[:-2]:
            final_translation += e

    return final_translation