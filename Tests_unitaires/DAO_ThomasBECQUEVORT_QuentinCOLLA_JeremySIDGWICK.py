#LINFO1103 - Tests unitaires
#Quentin COLLA, Jeremy SIDGWICK, Thomas BECQUEVORT

import unittest

def create_file(list, filename):
    """
    Crée un fichier avec comme lignes les éléments de la liste en paramètre
    """
    with open('test.txt', 'w') as f:
        for i in range(len(list)-1):
            f.write(list[i] + '\n')
        f.write(list[-1]) #On n'a pas de retour à la ligne inutile à la fin du fichier

def file_to_string(filename):
    """
    Retranscrit le contenu d'un fichier en un string
    """
    with open(filename, 'r') as f:
        string = ''
        for line in f:
            string += line
        return string

class Test(unittest.TestCase):

    def setUp(self):
        
        #Question 3
        self.P8014  = [ 'Bruxelles-Nord', 'Bruxelles-Central', 'Bruxelles-Midi', 'Liedekerke', 'Denderleeuw', 'Erembodegem', 'Alost' ]
        self.IC2238 = [ 'Bruxelles-Nord', 'Bruxelles-Central', 'Bruxelles-Midi', 'Liedekerke', 'Denderleeuw', 'Alost' ]
        self.S10    = [ 'Bruxelles-Nord', 'Bockstael', 'Jette', 'Berchem-Sainte-Agathe', 'Grand-Bigard', 'Dilbeek', 'Sint-Martens-Bodegem', \
             'Ternat', 'Essene Lombeek', 'Liedekerke', 'Denderleeuw', 'Erembodegem', 'Alost' ]

        #Question 4
        #On crée deux fichiers, un avec un format correct et l'autre avec un format incorrect
        create_file(['charles!3456', 'kim!5678 '], 'correct_test.txt')
        create_file(['alice!123', 'bob!erreur', 'charlie!5768!eve'], 'incorrect_test.txt')

        #Question 5a
        couloir1 = Couloir(9)
        couloir2 = Couloir(17.3)
        couloir3 = Couloir(3.0)
        
        #Question 5b
        l1 = Lampe("ampoule classique1",60)
        l2 = Lampe("ampoule classique2",60)
        l3 = Lampe("ampoule classique3",60)
        l4 = Lampe("ampoule classique4",60)
        p = Piece("Living",[l1, l2, l3, l4], 2)

    def test_approx_pi(self):
        self.assertEqual(approx_pi(0), 4.0, msg='Erreur avec la valeur 0')
        self.assertAlmostEqual(approx_pi(1), 2.666666, places=2, msg='Erreur avec la valeur 1')
        self.assertAlmostEqual(approx_pi(2), 3.466666, places=2, msg='Erreur avec la valeur 2')
        self.assertAlmostEqual(approx_pi(1000), 3.1425916543395442, places=5, msg='Erreur avec la valeur 1000')

    def test_repetition(self):
        self.assertEqual(repetition('cccdddd'), 4, msg='La fonction ne prend pas en compte le dernier caractère')
        self.assertEqual(repetition('ccccddd'), 4, msg='La fonction ne prend pas en compte le premier caractère')
        self.assertEqual(repetition('a'), 1, msg='Erreur avec une chaîne d\'un seul caractère')
        self.assertEqual(repetition('aAbBb'), 3, msg='Erreur avec des majuscules et des minuscules')
        self.assertEqual(repetition('éééaaé'), 3, msg='Erreur avec des accents')
        self.assertEqual(repetition('aabbbaa'), 3, msg='La fonction ne compte pas que les lettres consécutives')
        

    def test_common_path_at_end(self):
        self.assertEqual(common_path_at_end(self.P8014, self.S10), ['Liedekerke', 'Denderleeuw', 'Erembodegem', 'Alost'], msg='Test classique')
        self.assertEqual(common_path_at_end(self.P8014, self.IC2238), ['Alost'], msg='Test classique 2')
        self.assertEqual(common_path_at_end([], []), [], msg='Erreur avec deux listes vides')
        self.assertEqual(common_path_at_end([], self.P8014), [], msg='Test avec la première liste vide')
        self.assertEqual(common_path_at_end(self.P8014, []), [], msg='Test avec la deuxième liste vide')
        self.assertEqual(common_path_at_end(self.S10, self.S10), self.S10, msg='Test avec deux listes identiques')

    def test_load(self):
        #on vérifie que la fonction renvoie bien True ou False
        self.assertTrue(load('correct_test.txt', {'siegfried':1234}), msg='La fonction ne retourne pas True correctement')
        self.assertFalse(load('incorrect_test.txt', {'siegfried':1234}), msg='La fonction ne retourne pas False correctement')

        #On vérifie que le fichier est modifié seulement si la fonction retourne True
        corr_string = file_to_string('correct_test.txt') #Le contenu du fichier d'après la fonction
        inc_string = file_to_string('incorrect_test.txt')

        #On compare le contenu du fichier par rapport à ce qui est correct
        self.assertEqual(corr_string, 'charles!3456\nkim!5678\nsiegfried!1234', msg='La fonction n\'a pas modifié le message alors qu\'elle devait le faire')
        self.assertEqual(inc_string, 'alice!123\nbob!erreur\ncharlie!5768', msg='La fonction load a modifié le fichier alors qu\'elle ne devait pas le faire')
        
    def test_couloir_a(self):
        self.assertEqual(couloir1.lampe, 2, msg='La fonction ne retourne pas le bon nombre de lampes')
        self.assertEqual(couloir2.lampe, 4, msg='La fonction ne retourne pas le bon nombre de lampes')
        self.assertEqual(couloir3.lampe, 1, msg='La fonction ne retourne pas le bon nombre de lampes')

        self.assertEqual(couloir1.consommation, 0.0075, msg='Erreur dans le calcul de consommation')
        self.assertEqual(couloir2.consommation, 0.015, msg='Erreur dans le calcul de consommation')
        self.assertEqual(couloir3.consommation, 0.0, msg='Erreur dans le calcul de consommation')
        

    def test_couloir_b(self):
        self.assertEqual(p.consommation(), 0.02, msg='Test de base')
        p.ajouter_lampe(AmpouleBasse())
        self.assertEqual(pconsommation(), 0.021, msg='La fonction n\'ajoute pas les éléments correctement')
        l_removed = p.supprimer_lampe()
        self.assertEqual(l_removed, 14)
        self.assertEqual(p.consommation(), 0.016, msg='La fonction ne retire pas les éléments correctement')


if __name__ == '__main__':
    unittest.main()



