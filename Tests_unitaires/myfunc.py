def approx_pi(i):        # NE PAS EFFACER CETTE LIGNE
    """
    @pre:   i est un entier tel que i >= 0
    @post:  retourne une estimation de pi en sommant
            les i + 1 premiers termes de la série de Gregory-Leibniz
    """

    ### VOTRE REPONSE
    pi = 4.0
    multiplicateur = 0.0
    for j in range(i+1):
        if j % 2 == 0:   #Si j est pair
            #addition
            multiplicateur += 1 / (1 + (j * 2))
        else:            #Si j est impair
            #soustraction
            multiplicateur -= 1 / (1 + (j * 2))
    return (pi * multiplicateur)
            
            
        
def repetition(s):        # NE PAS EFFACER CETTE LIGNE
    """
    @pre:  s est une chaîne de caractères non-vide composée seulement de lettres.
    @post: Retourne la longueur de la plus longue séquence consécutive de lettres répétées
           dans la chaîne de caractères s, sans tenir compte de la casse des lettres.
    """
    s = s.upper()
    s += '0'
    biggest_count = 0
    count = 1
    idx = 0
    
    while s[idx] != '0':
        if s[idx] == s[idx + 1]:
            #Si le caractère est le même que le suivant
            count += 1
        else:
            #Si le caractère n'est pas le même que le suivant
            if biggest_count < count:
                biggest_count = count
            count = 1
        idx += 1
        
    s = s.rstrip('0')    #On retire le 0 pour récupérer la string d'origine    
    return biggest_count
        

def common_path_at_end(route1,route2) :        # NE PAS EFFACER CETTE LIGNE
    """
    @pre:   route1 et route2 sont deux listes de strings
    @post:  retourne une liste reprenant le plus grand suffixe
            commun entre route1 et route2. Autrement dit la plus longue sous-séquence
            en partant de la fin qui contient les mêmes éléments.
            route1 et route2 n'ont pas été modifiées.
    """
    route1.reverse()
    route2.reverse()
    route1.append('.')
    route2.append('.')
    common_path = []
    idx = 0
    nbr = 0    #Le nombre de stations que les deux routes ont en commun en partant de la fin
    while route1[idx] == route2[idx]:
        idx += 1
        nbr += 1
    
    for i in range(nbr):
        common_path.append(route1[i])
        
    common_path.reverse()
    return common_path


def format(filename):
    try:
        with open(filename, 'r') as f:
            lineFin = True
            while lineFin:
                line = f.readline()
                if line == '':
                    lineFin = False
                else:
                    line = line.strip().split('!')
                    if len(line) != 2:
                        return False
                    for elem in line[1]:
                        if elem not in '0123456789':
                            return False
                    if len(line[1]) != 4:
                        return False
                    if line[1][0] == '0':
                        return False
    except:
        return False
    return True

def load(filename, dictionnary):        # NE PAS EFFACER CETTE LIGNE
    """
    @pre    filename est le nom d'un fichier de texte,
            dictionnary est un dictionnaire avec :
                comme clés : des chaînes de caractères (les noms d'utilisateurs)
                comme valeurs : des entiers (un code pin associé à l'utilisateur)
    @post   Met à jour le dictionnaire à partir des clients repris dans le fichier de nom filename.
            Si une erreur se produit pendant la lecture du fichier, le dictionnaire n'est pas
            modifié.
            Retourne :
                True     si le fichier a été lu sans erreurs
                False    sinon
    """
    if not format(filename):
        return False
    else:
        with open(filename, 'r') as f:
            lineFin = True
            while lineFin:
                line = f.readline()
                if line == '':
                    lineFin = False
                else:
                    line = line.strip().split('!')
                    dictionnary[line[0]] = int(line[1])
    return True



            

    

        

