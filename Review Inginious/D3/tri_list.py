def trie_list(l):
    """
    trie_list([x1,x2,x3,...,xn])
    >>> [x1,xn,x2,xn-1,...]
    """
    for i in range(1, len(l)//2):
        elem = l.pop(-i)
        l.insert((i+1)*2, elem)
    return l


"""
Vous demandez d'écrire le corps de la fonction, mais il faut aussi écrire la définition.
Mise à part le fait que vous faites deux fois le test avec une liste vide et que vous ne faites pas de test avec une liste
entièrement négative ou décroissante, vos test me semblent complets et l'exercice est d'un bon niveau.
"""

print(trie_list([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))