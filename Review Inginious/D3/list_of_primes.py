def list_of_primes(n):
    list = []
    if n == 0:
        return []
    if n == 1:
        return [1]
    for i in range (1, n):
        isPrime = True
        for j in range (2, i):
            if i % j == 0:
                isPrime = False
        if isPrime:
            list.append(i)
    return list

print(list_of_primes(10))
print(list_of_primes(27))
print(list_of_primes(99))
print(list_of_primes(1))
print(list_of_primes(0))
print(list_of_primes(-3))





"""
Vous avez oublié de changer l'énoncé de la "sous-question", c'est le même que celui de votre premier exercice.
Il faut écrire la définition de la fonction malgré le fait que vous l'affichez au-dessus, ce n'est pas forcément clair.
Je ne comprends pas bien la différence entre vos deux premières séries de test, elles sont toutes les deux composées d'entiers positifs, je
ne pense pas que ces deux séries soient nécessaires ou doivent être séparées en deux fonctions. Aussi, vous testez les cas avec un paramètre positif et un négatif, 
mais vous ne testez pas l'argument 0 qui est tout aussi important.
"""