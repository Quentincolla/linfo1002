def short_word(str):
    occ = {}
    str = str.split(' ')
    for word in str:
        word = word.lower()
        if word not in '.,:' and len(word) < 3:
            occ[word] = occ.get(word, 0) + 1
    return occ

"""
Bon exercice en général, pensez peut-être à dire qu'il faut faire attention aux majuscules en début de phrase.
Cependant, je pense que vos tests ne sont pas assez complets. En cas d'erreur, l'étudiant ne sera pas précisément quel est le problème
dans son code. Vous devriez faire des tests plus "séparés" et éventuellement finir avec celui que vous avez actuellement (une grande histoire).
Le plus simple serait de faire un test pour vérifier que les ponctuations ne sont pas considérés comme des mots, vérifier qu'on ne prend pas en compte
les mots de plus de deux lettres, vérifier que le nombre d'occurrences est bien calculé, etc.
"""
            