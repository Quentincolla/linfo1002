class Student:

    def __init__(self, nom):
        self.nom = nom
        self.note = {}

    def noter(self,releve):
        """
        pre: releve est une liste de listes contenant les informations comme suit :
        [["matiere1",int],["matière2",int]]

        post self.note est mis a jour avec les données de releve
        """
        for notes in releve:
            self.note[notes[0]] = notes[1]


    def __str__(self):
        return str(self.nom)+"  "+str(self.note)

"""
Exercice un peu trop simple, il aurait pû être plus complexe en demandant une moyenne lorsqu'on a plusieurs notes
pour une même matière par exemple. Le fait qu'on met à jour la note d'une matière en supprimant la note précédente n'a pas
beaucoup de sens (à mon goût).
Vos tests sont peut-être un peu trop courts, vous devriez en faire un avec 4 ou 5 matières et plusieurs utilisations pour être sûr
que le programme de l'étudiant ne fonctionne pas que pour de petits cas.
(+ Faute dans l'énoncé : "si une matière apparait __dans plusieurs fois__ au cours des différentes utilisations")
"""