def register(file):
    """
        - Prenom1 Nom1
        Matiere1 XX
        Matiere2 XX
        Matière3 XX

        - Prenom2 Nom2
        Matiere1 XX
        Matiere4 XX
    """
    register = []
    student = []
    with open(file, 'r') as f:
        content = f.readlines()
        while '\n' in content:
            #remove all the '\n'
            content.remove('\n')
        
        for line in content:
            line = line[-1].split(' ')
            if len(line) == 3 and line[0] == '-':
                #it's a student 
                name = line[1] + ' ' + line[2]
                register.append(student)
                student = [name]
            elif len(line) == 2 and not line[1].isalpha():
                subject, score = line[0], line[1]
                student.append((subject, score))
            else:
                raise OSError
    try:
        register.remove([]) #Remove the first index which is an empty list
    except:
        raise OSError
    return register


"""
L'énoncé est peut-être un peu trop long et avec un peu trop d'exceptions et de "pièges" possibles (plusieurs retours à la ligne, 
un élève sans point, un fichier qu'on ne peut pas lire, des informations dans le désordre, etc). Je pense que vous devriez réduire un
petit peu la difficulté en retirant quelques éléments pour éviter de devoir rendre un code de plus de 100 lignes sur Inginious.
Vos tests semblent complets et efficaces.
"""
        