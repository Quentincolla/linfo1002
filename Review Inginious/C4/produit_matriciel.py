def produit_matriciel(matrice1, matrice2):
    if len(matrice1[0]) != len(matrice2):
        return None
    answer = []
    for i in range (len(matrice1)):
        line = []
        for j in range(len(matrice1[i])):
            elem = 0
            for k in range (len(matrice2[j])):
                elem += matrice1[i][j] * matrice2[j][k]
            line.append(elem)
        answer.append(line)
    return answer